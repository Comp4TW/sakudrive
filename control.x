/* server.x : definição da interface */
#define PROGRAM_NUMBER 0x11005602
#define VERSION_NUMBER 1

/* tipo de dado que será passado aos procedimentos remotos */

struct filedata {
    char data[1024];
    int size;
    filedata* next;
};

struct fileinfo {
    char name[255];
    filedata* file;
};

/* Definição da interface que será oferecida aos clientes */
program SAKUDRIVE_CONTROL_PROG {
    version SAKUDRIVE_CONTROL_VERSION {
    filedata API_LIST         ()         = 1;
         int API_REMOVE       (fileinfo) = 2;
         int API_SENDFILE     (fileinfo) = 3;
    fileinfo API_GETFILE      (fileinfo) = 4;
    }
    = VERSION_NUMBER;
}
= PROGRAM_NUMBER;