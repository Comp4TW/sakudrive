/* server.x : definição da interface */
#define PROGRAM_NUMBER 0x11005601
#define VERSION_NUMBER 1

/* tipo de dado que será passado aos procedimentos remotos */

struct filedata {
    char data[1024];
    int size;
    filedata* next;
};

struct fileinfo {
    char name[255];
    filedata* file;
};

/* Definição da interface que será oferecida aos clientes */
program SAKUDRIVE_SERVER_PROG {
    version SAKUDRIVE_SERVER_VERSION {
         int SIZE         ()         = 1;
    filedata LIST         ()         = 2;
         int REMOVE       (fileinfo) = 3;
         int SENDFILE     (fileinfo) = 4;
    fileinfo GETFILE      (fileinfo) = 5;
    }
    = VERSION_NUMBER;
}
= PROGRAM_NUMBER;