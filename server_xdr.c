/*
 * Please do not edit this file.
 * It was generated using rpcgen.
 */

#include "server.h"

bool_t
xdr_filedata (XDR *xdrs, filedata *objp)
{
	register int32_t *buf;

	int i;
	 if (!xdr_vector (xdrs, (char *)objp->data, 1024,
		sizeof (char), (xdrproc_t) xdr_char))
		 return FALSE;
	 if (!xdr_int (xdrs, &objp->size))
		 return FALSE;
	 if (!xdr_pointer (xdrs, (char **)&objp->next, sizeof (filedata), (xdrproc_t) xdr_filedata))
		 return FALSE;
	return TRUE;
}

bool_t
xdr_fileinfo (XDR *xdrs, fileinfo *objp)
{
	register int32_t *buf;

	int i;
	 if (!xdr_vector (xdrs, (char *)objp->name, 255,
		sizeof (char), (xdrproc_t) xdr_char))
		 return FALSE;
	 if (!xdr_pointer (xdrs, (char **)&objp->file, sizeof (filedata), (xdrproc_t) xdr_filedata))
		 return FALSE;
	return TRUE;
}
