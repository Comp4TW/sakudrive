## Alunos:

* Fernando Luiz Bonifácio de Oliveira
* Filipe Polina Affonso

## Compilando e Executando

O make produzirá 3 executáveis que devem ser executados da seguinte forma:
sakudrive_client
    ./sakudrive_client [ip do controler]

sakudrive_control
    ./sakudrive_control
    ** Deve existir na pasta um arquivo servers.list
    ** Este arquivo deverá ter o número de servidores na primeira linha e o ip
        dos servidores nas linhas seguintes, um ip por linha, com uma linha em
        branco no final do arquivo

sakudrive_server
    ./sakudrive_server
    ** Apenas uma instancia do servidor pode rodar em uma máquina, para testes
        recomenda-se o uso de máquinas virtuais
    ** O server irá disponibilizar os arquivos da pasta onde ele está rodando,
        portanto recomenda-se não rodar o server mesma pasta do executável.
        *** Exemplo:
            |-- Servidor/
              |-- Arquivos/
              | |-- file1.txt
              | |-- file2.pdf
              |-- Executáveis/
                |-- sakudrive_server
                |-- sakudrive_control
                |-- sakudrive_client

            cd Servidor/Arquivos
            ../Executáveis/sakudrive_server
