/*
 * This is sample code generated by rpcgen.
 * These are only templates and you can use them
 * as a guideline for developing your own functions.
 */

#include <unistd.h>
#include "server.h"

 int *
 size_1_svc(void *argp, struct svc_req *rqstp)
 {
    static int  result;

    int pfd[2], n;
    char str[1000];

    if (pipe(pfd) < 0) {
        printf("Oups, pipe failed.  Exiting\n");
        exit(-1);
    }

    n = fork();

    if (n < 0) {
        printf("Oups, fork failed.  Exiting\n");
        exit(-2);
    }
    else {
        if (n == 0) {
            close(pfd[0]);

            dup2(pfd[1], 1);
            close(pfd[1]);

            execlp("du", "du", "-s", NULL);
            printf("Oups, execlp failed.  Exiting\n"); /* This will be read by the  parent. */
            exit(-1); /* To avoid problem if execlp fails, especially if in a loop. */
        }
        else {
            close(pfd[1]);

            n = read(pfd[0], str, 10); 
            str[n] = '\0';

            close(pfd[0]);
            wait(&n); /* To avoid the zombie process. */

            if (n == 0) {
                result = atoi(str);
            }
            else {
                result = 0;
                printf("Oups, du or execlp failed.\n");
            }
        }
    }

    return &result;
}

filedata *
list_1_svc(void *argp, struct svc_req *rqstp)
{
    static filedata  result;

    int pfd[2], n;
    char str[1000];

    if (pipe(pfd) < 0) {
        printf("Oups, pipe failed.  Exiting\n");
        exit(-1);
    }

    n = fork();

    if (n < 0) {
        printf("Oups, fork failed.  Exiting\n");
        exit(-2);
    }
    else {
        if (n == 0) {
            close(pfd[0]);

            dup2(pfd[1], 1);
            close(pfd[1]);

            execlp("ls", "ls", "-1", NULL);
            printf("Oups, execlp failed.  Exiting\n"); /* This will be read by the  parent. */
            exit(-1); /* To avoid problem if execlp fails, especially if in a loop. */
        }
        else {
            close(pfd[1]);

            n = read(pfd[0], str, 1000);
            str[n] = '\0';

            close(pfd[0]);
            wait(&n); /* To avoid the zombie process. */

            if (n == 0) {
                struct filedata *raux = &result;
                char *pch;
                pch = strtok(str, "\n");                
                
                while(pch != NULL){
                    strcpy(raux->data, pch);
                    pch = strtok(NULL, "\n");
                    raux->next = malloc(sizeof(struct filedata));
                    raux = raux->next;
                }

                raux->next = NULL;

            }
            else {
                printf("Oups, ls or execlp failed.\n");
            }
        }
    }

    return &result;
}

int *
remove_1_svc(fileinfo *argp, struct svc_req *rqstp)
{
    static int  result;

    result = remove(argp->name);

    if (!result) {
        printf("Removendo Arquivo: %s\n", argp->name);
    }

     return &result;
 }

int *
sendfile_1_svc(fileinfo *argp, struct svc_req *rqstp)
{
    static int  result;

    FILE *pfile;
    printf("Recebendo Arquivo: %s\n", argp->name);
    pfile = fopen(argp->name, "wb");
    if(pfile == NULL){
        printf("Erro ao criar arquivo.\n");
        result = 1;
    
    }
    else{
        filedata *aux = argp->file;
        while(aux){
            fwrite(aux->data, sizeof(char), aux->size, pfile);
            aux = aux->next;
        }

        result = 0;
        fclose(pfile);
    }

     return &result;
}

fileinfo *
getfile_1_svc(fileinfo *argp, struct svc_req *rqstp)
{
    static fileinfo  result;

    FILE *pfile;
    pfile = fopen(argp->name, "rb");
    if(pfile == NULL){
        printf("Arquivo '%s' não existente :(\n", argp->name);
        result.file = NULL;
    }
    else{
        printf("Enviando Arquivo: %s\n", argp->name);
        filedata *aux;
        strcpy(result.name,argp->name);
        aux = malloc(sizeof(filedata));
        result.file = aux;
        
        int i;
        do{
            aux->size = fread(aux->data, sizeof(char), 1024, pfile);
            aux->next = malloc(sizeof(filedata));
            i = aux->size;
            aux = aux->next;
        }
        while(i==1024);
    
        aux->next = NULL;
        fclose(pfile);
    }
    return &result;
}
