# Parameters

CLIENT  = sakudrive_client
CONTROL = sakudrive_control
SERVER  = sakudrive_server

SOURCES_CLNT.c = 
SOURCES_CLNT.h = 
SOURCES_CTRL.c = 
SOURCES_CTRL.h = 
SOURCES_SVC.c = 
SOURCES_SVC.h = 
SOURCES.x = 

TARGETS_SVC.c = server_svc.c server_server.c server_xdr.c 
TARGETS_CTRL.c = control_svc.c control_server.c control_xdr.c server_clnt.c server_client.c 
TARGETS_CLNT.c = control_clnt.c control_client.c control_xdr.c 
TARGETS = server.h min_server.h server_xdr.c server_clnt.c server_svc.c server_client.c server_server.c control.h control_xdr.c control_clnt.c control_svc.c control_client.c control_server.c

OBJECTS_CLNT = $(SOURCES_CLNT.c:%.c=%.o) $(TARGETS_CLNT.c:%.c=%.o)
OBJECTS_CTRL = $(SOURCES_CTRL.c:%.c=%.o) $(TARGETS_CTRL.c:%.c=%.o)
OBJECTS_SVC = $(SOURCES_SVC.c:%.c=%.o) $(TARGETS_SVC.c:%.c=%.o)
# Compiler flags 

CFLAGS += -g -w 
LDLIBS += -lnsl
RPCGENFLAGS = 

# Targets 

all : $(CLIENT) $(CONTROL) $(SERVER)

$(TARGETS) : $(SOURCES.x) 
	rpcgen $(RPCGENFLAGS) $(SOURCES.x)

$(OBJECTS_CLNT) : $(SOURCES_CLNT.c) $(SOURCES_CLNT.h) $(TARGETS_CLNT.c) 

$(OBJECTS_CTRL) : $(SOURCES_CTRL.c) $(SOURCES_CTRL.h) $(TARGETS_CTRL.c) 

$(OBJECTS_SVC) : $(SOURCES_SVC.c) $(SOURCES_SVC.h) $(TARGETS_SVC.c) 

$(CLIENT) : $(OBJECTS_CLNT) 
	$(LINK.c) -o $(CLIENT) $(OBJECTS_CLNT) $(LDLIBS) 

$(CONTROL) : $(OBJECTS_CTRL) 
	$(LINK.c) -o $(CONTROL) $(OBJECTS_CTRL) $(LDLIBS)

$(SERVER) : $(OBJECTS_SVC) 
	$(LINK.c) -o $(SERVER) $(OBJECTS_SVC) $(LDLIBS)

 clean:
	$(RM) core $(TARGETS) $(OBJECTS_CLNT) $(OBJECTS_CTRL) $(OBJECTS_SVC) $(CLIENT) $(CONTROL) $(SERVER)

